
USERNAME=$1
TOKEN_FRONT=$2
WORKDIR=/home/ubuntu

function cleanall {
  cd ${WORKDIR} || exit 1
  echo "[DEPLOY]: CLEAN - deteniendo los procesos del manager pm2"
  /usr/bin/pm2 stop all
  /usr/bin/pm2 delete all
  echo "[DEPLOY]: CLEAN - borrando archivos del proyecto"
  rm -rf sistema-tutoria || exit 1
}

function clone {
  echo "[DEPLOY]: CLONE - inicio del proceso de clonacion"
  echo "[DEPLOY]: CLONE - creando carpeta sistema-tutoria"
  cd ${WORKDIR} || exit 1
  mkdir sistema-tutoria
  cd    sistema-tutoria || exit 1
  # clonando repo frontend con token de gitlab
  echo "[DEPLOY]: CLONE - clonando repositorio tutoria-frontend"
  git clone https://"${USERNAME}":"${TOKEN_FRONT}"@gitlab.com/isolation-software/sistema-tutorias/tutoria-backend.git "tutoria-frontend"
  # copiando archivo .env
  echo "[DEPLOY]: CLONE - copiando archivo .env a tutoria-frontend"
  cp ${WORKDIR}/.env ${WORKDIR}/sistema-tutoria/tutoria-frontend
}

function run {
  echo "[DEPLOY]: RUN - inicio del proceso de arranque del frontend"
  cd ${WORKDIR}/sistema-tutoria/tutoria-frontend || exit 1
  echo "[DEPLOY]: RUN - instalando dependencias"
  npm install
  echo "[DEPLOY]: RUN - construyendo archivos de nuxt"
  npm run build
  echo "[DEPLOY]: RUN - corriendo sistema en segundo plano"
  /usr/bin/pm2 start npm -- start
  echo "[DEPLOY]: RUN - despliegue completado"
}

function main {
  cleanall
  clone
  run
}

main
