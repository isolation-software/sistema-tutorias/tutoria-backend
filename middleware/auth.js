export default function ({ redirect, route, store }) {
  let root = route.path.split('/')[1]
  if (!store.state.token) {
    return redirect('/')
  } else {
    //ESTO DEPENDE DE LOS NOMBRES DE LAS CARPETAS ASIGNADAS EN PAGES
    let paths = {
      admin: 0,
      coordinador: 1,
      tutor: 2,
      alumno: 3,
    }
    if (store.state.role){
      if (paths[root] && paths[root] !== store.state.role.idRole){
        return redirect('/roles')
      }
    } else if (root !== 'roles') {
        return redirect('/roles')
    }
  }
}
  