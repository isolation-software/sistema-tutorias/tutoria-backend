
function isValidEmail(email) {
  const emailRegex = /.+@.+\..+/
  return emailRegex.test(email);
}

function isEmptyText(text) {
  return text===null || text===undefined || text.trim()==="";
}

function minText(text, min) {
  return text.length >= min;
}

function maxText(text, max) {
  return text.length <= max;
}

export {isValidEmail, isEmptyText, minText, maxText};
