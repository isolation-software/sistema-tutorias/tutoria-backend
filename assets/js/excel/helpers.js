import XLSX from "xlsx";

function isExcel(file) {
  return file!==undefined && file!==null &&
    (file.name.toLowerCase().endsWith('xlsx') || file.name.toLowerCase().endsWith('xls'));
}


export function exportExcel(data, filename, sheetname=null) {
  sheetname = sheetname ? sheetname : filename;
  const worksheet = XLSX.utils.aoa_to_sheet(data);
  let workbook = XLSX.utils.book_new();

  XLSX.utils.book_append_sheet(workbook, worksheet, sheetname);
  XLSX.writeFile(workbook, filename)
}

export default isExcel;
