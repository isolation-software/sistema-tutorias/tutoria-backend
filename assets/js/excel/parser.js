import XLSX from 'xlsx'


function processExcel (excelFile, headerPosition) {
  let workbook = XLSX.read(excelFile, { type: 'binary' });
  let firstSheetName = workbook.SheetNames[0];
  let sheet = XLSX.utils.sheet_to_json(workbook.Sheets[firstSheetName], {header: headerPosition});
  return sheet
}

const excelParser = {
  importFile: function (file, headerPosition = 1) {
    return new Promise((resolve, reject) => {
      if (file) {
        let reader = new FileReader();
        reader.onload = e => resolve(processExcel(e.target.result, headerPosition));
        reader.readAsBinaryString(file);

      } else {
        reject(new Error('No se ha podido procesar el archivo Excel'));
      }
    });
  },

  importFileRaw: function (file) {
    return new Promise((resolve, reject) => {
      if (file) {
        let reader = new FileReader();
        reader.onload = e => resolve(processExcel(e.target.result));
        reader.readAsBinaryString(file);

      } else {
        reject(new Error('No se ha podido procesar el archivo Excel'));
      }
    });
  }
}

export default excelParser;
