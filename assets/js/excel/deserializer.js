
const excelDeserializer = {

  // replace se usa para reemplazar las cabeceras por los nombre que se usaran
  // en el json a enviar a backend
  deserialize: function(table, replace=null) {
    const headers = table[0];
    replace = !replace ? headers : replace;
    let rows = table.slice(1);
    let data = [];
    for (let r=0; r<rows.length; r++) {
      let obj = {}
      let row = rows[r]
      for (let h=0; h<replace.length; h++) {
        obj[replace[h]] = row[h] ? row[h].toString() : null
      }
      data.push(obj)
    }

    return data;
  }
}

export default  excelDeserializer;
