export default {
  correo:{
    label: 'Correo electrónico',
    counter: '',
    rules: [
      v => !!v || 'El e-mail es requerido',
      v => /.+@.+\..+/.test(v) || 'El correo debe ser valido',
    ],
  },
  codigo:{
    label: 'Código',
    type: 'number',
    counter: '8',
    rules: [
      v => !!v || 'El codigo es requerido',
      v => (v && v.length <= 8) || 'El código debe tener menos de 8 caracteres.',
    ],
  },
  nombres:{
    label: 'Nombres',
    counter: '150',
    rules: [
      v => !!v || 'El nombre es requerido',
      v => (v && v.length <= 150) || 'El nombre debe tener menos de 150 caracteres.',
    ],
  },
  apellidoPat:{
    label: 'Apellido paterno',
    counter: '150',
    rules: [
      v => !!v || 'El apellido paterno es requerido',
      v => (v && v.length <= 150) || 'El apellido debe tener menos de 150 caracteres.',
    ],
  },
  apellidoMat:{
    label: 'Apellido materno',
    counter: '150',
    rules: [
      v => !!v || 'El apellido materno es requerido',
      v => (v && v.length <= 150) || 'El apellido debe tener menos de 150 caracteres.',
    ],
  },
  telefono:{
    label: 'Teléfono',
    type: 'number',
    counter: '9',
    rules: [
      v => (v.length <= 9) || 'El teléfono debe tener menos de 9 caracteres.',
    ],
  }
}