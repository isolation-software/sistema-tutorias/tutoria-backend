
const tokenTool = {
  getPayload(token) {
    if (process.browser) {
      try {
        let payload =JSON.parse(window.atob(token.split('.')[1]));
        return payload;
      } catch (error) {
        console.error(error);
        return null;
      }
    }
    return null
  },

  saveToken(token) {
    window.localStorage.tokenST = token
  },

  deleteToken() {
    if (process.browser) window.localStorage.tokenST = null
  }
};

export default tokenTool;
