
export default function timeFormat (value) {
    if(!value) return '-';
    let options = { year: 'numeric', month: 'long', day: 'numeric', hour12: 'true',timeStyle: 'short', dateStyle: 'long'}
    return new Date(value).toLocaleTimeString('es', options);
}
