import bulkValidator from "./validation";
import excelDeserializer from "../excel/deserializer";
import { exportExcel } from "../excel/helpers";
import XLSX from 'xlsx'

const replace  = ['codigo', 'nombres', 'apellidoPat'      , 'apellidoMat'    , 'correo', 'telefono', 'codUnidad'];
const headers  = ['CÓDIGO', 'NOMBRES', 'APELLIDO PATERNO', 'APELLIDO MATERNO', 'CORREO', 'TELÉFONO', 'CÓDIGO DE ESPECIALIDAD'];
const noempty  = ['CÓDIGO', 'NOMBRES', 'APELLIDO PATERNO', 'APELLIDO MATERNO', 'CORREO', null      , 'CÓDIGO DE ESPECIALIDAD'];
const norepeat = ['CÓDIGO', null     , null               , null             , 'CORREO', null      , null];

const studentTemplateFilename = "Alumnos.xlsx";
const studentNotRegisterdFilename = "Alumnos_no_registrados.xlsx";

const studentBulker = {

  deserialize: function(table) {
    return excelDeserializer.deserialize(table, replace);
  },

  validateHeaders: function (data) {
    return bulkValidator.validateHeaders(data, headers);
  },

  validateNoEmptyColumnValues: function(data) {
    return bulkValidator.validateNoEmptyColumnValues(data, noempty);
  },

  validateNoRepeteatedValues: function(data) {
    return bulkValidator.validateNoRepeteatedValues(data, norepeat);
  },

  downloadTemplate: function() {
    // exportExcel([headers], studentTemplateFilename, "alumnos");
  },

  downloadExcelNotRegistered2: function(students) {
    let data = [];
    data[0] = [...headers, 'MENSAJE DEL SISTEMA'];
    students.sort((s1, s2) => (s1.codigo > s2.codigo) ? 1 : -1);
    students.forEach((student, index) => {
      let row = [];
      replace.forEach(header => row.push(student[header]));
      row.push(student['mensaje']);
      data[index+1] = row;
    });
    exportExcel(data, studentNotRegisterdFilename, "no registrados");
  },

  downloadExcelNotRegistered: function(students, template) {
    let workbook  = XLSX.readFile(template)
    let sheet_name = workbook.SheetNames[0]
    let worksheet = workbook.Sheets[sheet_name]

    // let data = []
    // data[0] = [...headers, 'MENSAJE DEL SISTEMA'];
    // students.sort((s1, s2) => (s1.codigo > s2.codigo) ? 1 : -1);
    // students.forEach((student, index) => {
    //   let row = [];
    //   replace.forEach(header => row.push(student[header]));
    //   row.push(student['mensaje']);
    //   data[index+1] = row;
    // });

    // const lcol = ['A','B','C','D','E','F','G','H','I','J']
    // for (let r=0, r=lrow.length; r++) {
    //
    // }

    workbook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(workbook, worksheet, "alumnos no registrados");
    XLSX.writeFile(workbook, "no_registrados.xlsx")
  }
}

export default studentBulker;
