
const bulkValidator = {
  validateHeaders: function(data, headers) {
    let res = {ok: true, msg: 'Formato correcto del archivo excel', headers: []};
    for (let i=0; i<headers.length; i++) {
      if (data[0][i] !== headers[i]) {
        return {ok: false, msg: 'Formato incorrecto de columnas en el archivo excel'};
      }
    }
    return res;
  },

  validateNoEmptyColumnValues(data, headers) {
    let res = {ok: true, msg: 'Datos completos en columnas obligatorias', headers:[]};
    const rows = data.slice(1);
    for (let h=0; h<headers.length; h++) {
      if (headers[h] !== null) {
        for (let r=0; r<rows.length; r++) {
          if (!rows[r][h]) {
            return {ok: false, msg: `Datos incompletos en columna ${headers[h]}`, headers: [headers[h]]};
          }
        }
      }
    }
    return res;
  },

  validateNoRepeteatedValues(data, headers=[]) {
    let res = {ok: true, msg: 'Datos no repetidos en columnas', headers:[]};
    const rows = data.slice(1);
    for (let h=0; h<headers.length; h++) {
      if (headers[h] !== null) {
        let values = []
        let value = null
        for (let r=0; r<rows.length; r++) {
          value = rows[r][h]
          if (values.includes(value)) return {ok: false, msg: `Valores repetidos en la columna ${headers[h]}`}
          values.push(value)
        }
      }
    }

    return res
  }
}

export default bulkValidator;
