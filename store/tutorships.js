import timeFormat from "../assets/js/time-format";

export const state = () => ({
  tutorias: [],
  tutoria: {
    idTutoria: null,
    nombre: 'Cargando...',
    descripcion: 'Cargando...',
    numSesiones: 'Cargando...',
    tipoTutoria: {
      tutorFijo: true,
      tutorAsignado: true,
      individual: true,
      obligatoria: true
    },
  },

  totalTutores: 0,
  tutores: [],
  tutorsTableHeaders: [
    {
      text: 'Nombres',
      align: 'start',
      sortable: true,
      value: 'nombre',
    },
    {
      text: 'Código',
      align: 'start',
      sortable: true,
      value: 'codigo',
    }
  ],
  tutorsTableHeaders2: [
    {
      text: 'Nombres',
      align: 'start',
      sortable: true,
      value: 'nombre',
    },
    {
      text: 'Código',
      align: 'start',
      sortable: true,
      value: 'codigo',
    },
    {
      text: 'Acciones',
      align: 'center',
      sortable: false,
      value: 'actions',
    }
  ],
  showModalTutors: false,
  modalTutors: [],
  updatingTutorsTable: false,

  totalAlumnos: 0,
  alumnos: [],
  studentTableHeaders: [
    {
      text: 'Nombres',
      align: 'start',
      sortable: true,
      value: 'nombre',
    },
    {
      text: 'Código',
      align: 'start',
      sortable: true,
      value: 'codigo',
    }
  ],
  studentTableHeaders2: [
    {
      text: 'Nombres',
      align: 'start',
      sortable: true,
      value: 'nombre',
    },
    {
      text: 'Código',
      align: 'start',
      sortable: true,
      value: 'codigo',
    },
    {
      text: 'Acciones',
      align: 'center',
      sortable: false,
      value: 'actions',
    }
  ],
  showModalStudents: false,
  modalStudents: [],
  updatingStudentsTable: false,

  loading: false,
  error: false,
  success: false,

  sessionsTableHeaders: [
    {
      text: 'Código del alumno',
      align: 'start',
      sortable: false,
      value: 'codigo',
    },
    { text: 'Alumno', value: 'nombre' },
    { text: 'Tutor', value: "ultimaSesion.tutorAsignado.nombre" },
    { text: 'Fecha de atención', value: 'fechaFormato' },
    { text: 'Estado', value: 'ultimaSesion.estado' },
  ],
  sesiones: [],
  sesion: {
    idAlummno: null,
    codigo: "",
    nombre: "",
    ultimaSesion: {},
  },
})

export const mutations = {
  UPDATE_TUTORSHIPS(state, tutorships) {
    Vue.set(state, 'tutorships', [...tutorships]);
  },
  UPDATE_TUTORSHIP(state, payload) {

    let tutoria = payload.tutoria;
    state.idTutoria = tutoria.idTutoria;
    state.tutoria.nombre = tutoria.nombre;
    state.tutoria.descripcion = tutoria.descripcion;
    state.tutoria.numSesiones = tutoria.numSesiones;
    state.tutoria.tipoTutoria = tutoria.tipoTutoria;
    state.totalTutores = tutoria.totalTutores;
    state.totalAlumnos = tutoria.totalAlumnos;

    state.alumnos.splice(0, state.alumnos.length);
    tutoria.alumnos.forEach(a => state.alumnos.push(a));

    state.tutores.splice(0, state.tutores.length);
    tutoria.tutores.forEach(t => state.tutores.push(t));
    this.tutores = tutoria.tutores;
  },
  CLEAN_ALL_TUTORSHIP(state) {
    state.tutores.splice(0, state.tutores.length);
    state.alumnos.splice(0, state.alumnos.length);
    state.tutoria.idTutoria = null;
    state.tutoria.nombre = 'Cargando...';
    state.tutoria.descripcion = 'Cargando...';
    state.tutoria.numSesiones = 'Cargando...';
  },
  RUN_LOADING(state) {
    if (!state.loading) state.loading = true;
  },
  STOP_LOADING(state) {
    if (state.loading) state.loading = false;
  },

  UPDATE_STUDENTS(state, payload) {
    state.alumnos.splice(0, state.alumnos.length);
    payload.alumnos.forEach(alumno => state.alumnos.push({ ...alumno }));
    state.totalAlumnos = state.alumnos.length;
  },
  UP_LOADING_UPDATE_STUDENTS_TABLE(state) {
    state.updatingStudentsTable = true;
  },
  DOWN_LOADING_UPDATE_STUDENTS_TABLE(state) {
    state.updatingStudentsTable = false;
  },
  CLOSE_STUDENTS_MODAL(state) {
    state.showModalStudents = false;
  },
  SHOW_STUDENTS_MODAL(state) {
    state.showModalStudents = true;
  },
  UPDATE_MODAL_SEARCH_STUDENTS(state, payload) {
    try {
      state.modalStudents.splice(0, state.modalStudents.length);
      payload.students.forEach(a => state.modalStudents.push({ ...a }));
    } catch (error) {
      console.error(error);
    }
  },

  UPDATE_TUTORS(state, payload) {
    state.tutores.splice(0, state.tutores.length);
    payload.tutores.forEach(alumno => state.tutores.push({ ...alumno }));
    state.totalTutores = state.tutores.length;
  },
  UP_LOADING_UPDATE_TUTORS_TABLE(state) {
    state.updatingTutorsTable = true;
  },
  DOWN_LOADING_UPDATE_TUTORS_TABLE(state) {
    state.updatingTutorsTable = false;
  },
  CLOSE_TUTORS_MODAL(state) {
    state.showModalTutors = false;
  },
  SHOW_TUTORS_MODAL(state) {
    state.showModalTutors = true;
  },
  UPDATE_MODAL_SEARCH_TUTORS(state, payload) {
    try {
      state.modalTutors.splice(0, state.modalTutors.length);
      payload.tutors.forEach(t => state.modalTutors.push({ ...t }));
    } catch (error) {
      console.error(error);
    }
  },


  UPDATE_SESSIONS(state, payload) {
    state.sesiones.splice(0, state.sesiones.length);
    payload.alumnos.forEach(a => {
      let fechaFormato = a.ultimaSesion ? timeFormat(a.ultimaSesion.fechaInicio) : '-';
      state.sesiones.push({ ...a, fechaFormato: fechaFormato })
    });
  },
  UPDATE_SESSION(state, payload) {
    let sesion = payload.sesion;
    state.sesion.idAlumno = sesion.idAlumno;
    state.sesion.codigo = sesion.codigo;
    state.sesion.nombre = sesion.nombre;
    state.sesion.ultimaSesion = sesion.ultimaSesion;
  },
  CLOSE_SESSION_MODAL(state) {
    state.showSessionModal = false;
  },
  SHOW_SESSION_MODAL(state) {
    state.showSessionModal = true;
  },
  CLEAN_CURRENT_SESSION(state) {
    state.sesion.idAlummno = null;
    state.sesion.codigo = "";
    state.sesion.nombre = "";
    state.sesion.ultimaSesion = {};
  },
}

export const actions = {
  search(context, payload) {
    try {
      const idUnit = context.rootGetters['roles/getCoordinatorSelectedUnit'];
      context.$api.getTutorships(idUnit, payload.searchQuery)
        .then(response => context.commit('UPDATE_TUTORSHIPS', response.data.data))
        .catch(error => console.error(error));
    } catch (error) {
      console.error(error)
    }
  },
  selectTutorship(context, payload) {
    const idTutorship = payload.idTutorship;
    context.commit('RUN_LOADING');
    this.$api.getTutorship(idTutorship)
      .then(response => {
        context.commit('UPDATE_TUTORSHIP', { tutoria: response.data.data })
        context.commit('STOP_LOADING');
      })
      .catch(error => console.error(error));
  },
  cleanAllTutorshipData(context) {
    context.commit('CLEAN_ALL_TUTORSHIP');
  },

  searchSessions(context, payload) {
    context.commit('RUN_LOADING');
    this.$api.getTutoshipSessions(payload.idTutorship)
      .then(response => {
        context.commit('UPDATE_SESSIONS', response.data.data);
        context.commit('STOP_LOADING');
      })
      .catch(error => console.error(error));
  },
  showSessionModal(context) {
    context.commit('CLEAN_CURRENT_SESSION');
    context.commit('SHOW_SESSION_MODAL');
  },
  closeSessionModal(context) {
    context.commit('CLOSE_SESSION_MODAL');
  },

  searchStudentsToAddInTutorship(context, payload) {
    try {
      return this.$api.getStudentsToAddInTutorship(payload.idTutorship, payload.searchQuery)
        .then(response => {
          context.commit('UPDATE_MODAL_SEARCH_STUDENTS', { students: response.data.data });
        })
        .catch(error => console.error(error));
    } catch (error) {
      console.error(error)
    }
  },
  registerStudentsInTutorship(context, payload) {
    let idStudents = payload.students.map(student => student.idAlumno);
    return this.$api.addStudentsInTutorship(payload.idTutorship, idStudents)
      .then(response => {
        return response;
      })
      .catch(error => console.error(error));
  },
  updateStudentsInTutorship(context, payload) {
    context.commit('UP_LOADING_UPDATE_STUDENTS_TABLE');
    return this.$api.getStudentsInTutorship(payload.idTutorship, payload.searchQuery)
      .then(response => {
        context.commit('UPDATE_STUDENTS', { alumnos: response.data.data });
        context.commit('DOWN_LOADING_UPDATE_STUDENTS_TABLE');
      }).catch(error => console.error(error));
  },
  closeStudentsModal(context) {
    context.commit('CLOSE_STUDENTS_MODAL');
  },
  showStudentsModal(context) {
    context.commit('SHOW_STUDENTS_MODAL');
  },

  searchTutorsToAddInTutorship(context, payload) {
    try {
      return this.$api.getTutorsToAddInTutorship(payload.idTutorship, payload.searchQuery)
        .then(response => {
          context.commit('UPDATE_MODAL_SEARCH_TUTORS', { tutors: response.data.data });
        })
        .catch(error => console.error(error));
    } catch (error) {
      console.error(error)
    }
  },
  registerTutorsInTutorship(context, payload) {

    let idTutors = payload.tutors.map(tutor => tutor.idTutor);
    return this.$api.addTutorsInTutorship(payload.idTutorship, payload.idUnit, idTutors)
      .then(response => {
        return response;
      })
      .catch(error => console.error(error));
  },
  updateTutorsInTutorship(context, payload) {
    context.commit('UP_LOADING_UPDATE_TUTORS_TABLE');
    return this.$api.getTutorsInTutorship(payload.idTutorship, payload.searchQuery)
      .then(response => {
        context.commit('UPDATE_TUTORS', { tutores: response.data.data });
        context.commit('DOWN_LOADING_UPDATE_TUTORS_TABLE');
      }).catch(error => console.error(error));
  },
  closeTutorsModal(context) {
    context.commit('CLOSE_TUTORS_MODAL');
  },
  showTutorsModal(context) {
    context.commit('SHOW_TUTORS_MODAL');
  },
}
