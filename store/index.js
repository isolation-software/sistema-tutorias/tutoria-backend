import * as Cookie from 'js-cookie'
import * as CookieParser from 'cookieparser'
import tokenTool from "../assets/js/token-tool";

//state is a method that returns an object
export const state = () => ({
    error:null,
    tutorship:{
        name: '',
        tutors:[]
    },
    names:{ //para pasar nombres de una pagina a otra y no tener q llamar al api de nuevo :u
      tutor: ''
    },
    token:null,
    //el usuario se setea en el login
    user:null,
    //el rol elegido se setea en la vista de roles
    role:null,
})

export const mutations = {
  setToken(state, token){
    state.token = token
  },

  setUser (state, user) {
    state.user = user
  },

  changeRole (state, newRole) {
    Cookie.set('currentRol', newRole )
    state.role = newRole
  },

  updateCurrentTutorship (state, newTutorship, newTutors) {
    state.tutorship.name = newTutorship.nombre;
    state.tutorship.tutors = newTutors;
  },

  updateName (state, newName) {
    state.names[newName.index] = newName.value; //newName = {index: 'tutor', value: 'Tutor A'}
  },

  cleanError(state){
    state.error = null
  },

  setError (state, error) {
    let errorObj = {}
    if(error.response){
        errorObj.status = error.response.status
        errorObj.msg= error.response.data.message //FALTA PROBAR
    } else if (error.request){
        errorObj.status = ''
        errorObj.msg= 'Error de conexion' //FALTA PROBAR
    } else {
        errorObj.status = ''
        errorObj.msg= error.message //FALTA PROBAR
    }
    state.error = errorObj
  }
}

export const actions = {
  login({commit}, newData){
    let { usuario, ...ids } = newData
    let token = 1234
    Cookie.set('token',token)
    Cookie.set('user',usuario)
    Cookie.set('ids', ids )
    commit('setToken', token)
    commit('setUser', usuario)
    commit('roles/setRoles',ids)
  },
  logout({commit}){
    Cookie.remove('token')
    Cookie.remove('user')
    Cookie.remove('ids')
    Cookie.remove('currentRol')
    commit('setToken', null)
    tokenTool.deleteToken()
    //commit('setUser', null)
    //commit('roles/clearRoles')
  },

  nuxtServerInit ({ commit }, { req }) {
    let token = null
    if (req.headers.cookie){
      const parsed = CookieParser.parse(req.headers.cookie)
      let user,ids,currentRol
      try {
        if (parsed.currentRol && Object.entries(parsed.currentRol).lenght != 0) {
          currentRol = JSON.parse(parsed.currentRol)
          commit('changeRole',currentRol)
        }
        if(parsed.token && Object.entries(parsed.token).lenght != 0) {
          token = JSON.parse(parsed.token)
          commit('setToken', token)
        }
        if(parsed.user && Object.entries(parsed.user).lenght != 0) {
          user = JSON.parse(parsed.user)
          commit('setUser', user)
        }
        if(parsed.ids && Object.entries(parsed.ids).lenght != 0) {
          ids = JSON.parse(parsed.ids)
          commit('roles/setRoles',ids)
        }
      } catch (e) {
        //commit('setError',e)
        this.$store.commit("setError", e);
      }
    }
  },
}
