export const state = () => ({
    userRoles:[],
    //no cambiar, o preguntar antes de cambiar algo aqui pls
    administrador:{
        id:null,
        idUnit: null,
        idRole: 0, 
        name: "Administrador General",
        image: 'images/women2.png',
        text: 'administrador General',
        link: '/admin/facultades',
        options: [
          { title: "Roles asignados", link: "/roles" },
          { title: "Facultades", link: "/admin/facultades" },
          { title: "Coordinadores", link: "/admin/coordinadores" },
          {title:"Universidad",link:"/admin/universidad"},
        ],
      },
    coordinador:{
      id: null,
      units: [],
      idRole: 1,
      unitSelected: null,
      name:'Coordinador de ',
      image: 'images/women1.png',
      text: 'coordinador de unidad',
      link: '/coordinador/tutorias',
      options: [
            { title: "Roles asignados", link: "/roles" },
            { title: "Tutorías", link: "/coordinador/tutorias" },
            { title: "Tutores", link: "/coordinador/tutor" },
            { title: "Alumnos", link: "/coordinador/alumnos" },
            { title: "Reportes", link: "/coordinador/reportes" },
            { title: "Programas", link: "/coordinador/programas" }, //esto se quita cuando es coor de programa
            { title: "Coordinadores", link: "/coordinador/coordinadores" }, // esto se quita cuando es coor de programa
      ],
    },
    tutor:{
      id:null,
      idUnit: null,
      idRole: 2,
      permission:null,
      name: "Tutor",
      image: 'images/men2.png',
      text: 'tutor',
      link: '/tutor/tutorias',
      options: [
          { title: "Roles asignados", link: "/roles" },
          { title: "Tutoría al momento", link: "/tutor/momento"},
          { title: "Citas", link: "/tutor/tutorias" },
          { title: "Seguimiento de sesiones", link : "/tutor/seguimiento"},
          { title: "Horario de Disponibilidad", link: "/tutor/horario" },
      ]
    },
    alumno:{
      id:null,
      idUnit: null,
      idRole: 3,
      name: "Alumno",
      image: 'images/men1.png',
      text: 'alumno',
      link: '/alumno/citas',
      options: [
          { title: "Roles asignados", link: "/roles" },
          { title: "Mis citas", link: "/alumno/citas" },
          { title: "Mis tutorías", link: "/alumno/tutorias" },
      ]
    }
})

export const mutations = {
    clearRoles(state){
      state.userRoles = []
      state.administrador.id = null
      state.coordinador.id = null
      state.coordinador.units = null
      state.tutor.id = null
      state.alumno.id = null
    },
    setRoles(state, obj) {
      if (obj.idAdministrador) {
        state.administrador.id = obj.idAdministrador
        state.userRoles.push(state.administrador)
      }
      if (obj.idCoordinador) {
        state.coordinador.id = obj.idCoordinador
        state.coordinador.units = obj.unidadesCoordinador
        state.userRoles.push(state.coordinador)
      }
      if (obj.idTutor) {
        state.tutor.id = obj.idTutor
        state.tutor.permission = obj.tutor.tienePermiso
        state.userRoles.push(state.tutor)
      }
      if (obj.idAlumno) {
        state.alumno.id = obj.idAlumno
        state.alumno.idUnit = obj.unidadAlumno.idUnidad
        state.userRoles.push(state.alumno)
      }
    },
}

export const getters = {
  getCoordinatorSelectedUnit: state => {
    return state.coordinator.unitSelected;
  }
}
