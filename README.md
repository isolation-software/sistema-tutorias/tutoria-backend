# tutoria-frontend

> frontend de sistema de tutorias

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
Nuxt structure information
---------

#### Pages
Routes for application.

#### Assets
Uncompiled assets just like Sass files.

#### Components
Vue.js components.

#### Layout
Template for default pages.

#### Middleware
Define functions that can be run rendering a page or a group of pages.

#### Plugins
JS plugins that you want to run before instantiating the route vue instance.. 

#### Static
Files automatically be mapped to the server root, images as logos

#### Store
For Vuex store
