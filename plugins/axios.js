import repository from '@/api/repository'


export default ({ $axios, redirect }, inject) => {
  $axios.setBaseURL(process.env.apiBaseUrl)

  $axios.onRequest(config => {
    
  })

  $axios.interceptors.request.use((config) => {
    if (process.browser){
      const token = window.localStorage.tokenST;
      if (token) config.headers['Authorization'] = token;
      if(config.url.split('/')[0] == 'reportes'){
        config.responseType = 'blob';
      }
      return config;
    }
    return config;
  });

  $axios.onError(error => {
    const code = parseInt(error.response && error.response.status)
    if (code === 400) {
      //redirect('/400')
      
    } else if(code === 500) {
      //redirect('/sorry')
      
    }
  })

  const api = repository($axios)
  inject('api', api)
}
