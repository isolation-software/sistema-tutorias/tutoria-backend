import Vue from 'vue'
import Alert from '@/components/Alert'
import Title from '@/components/Title'
import Subtitle from '@/components/Subtitle'
import EditButton from '@/components/EditButton'
import BackButton from '@/components/BackButton'
import CancelButton from '@/components/CancelButton'
import PrimaryButton from '@/components/PrimaryButton'
import SecondaryButton from '@/components/SecondaryButton'
import Content from '@/components/Content'

Vue.component('st-alert',Alert)
Vue.component('st-title',Title)
Vue.component('st-subtitle',Subtitle)
Vue.component('st-edit-btn',EditButton)
Vue.component('st-back-btn',BackButton)
Vue.component('st-cancel-btn',CancelButton)
Vue.component('st-primary-btn',PrimaryButton)
Vue.component('st-secondary-btn',SecondaryButton)
Vue.component('st-content',Content)
