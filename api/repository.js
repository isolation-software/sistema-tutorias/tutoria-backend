export default (axios) => {
  return ({
    //UNIVERSITY
    addUniversity:(dictionary,files) => axios.post('universidad',dictionary,files),

    // USERS
    verifyEmail: (email) => axios.get(`usuarios/validacion/alumno?correo=${email}`),
    verifyTutorEmail: (email,idUnit) => axios.get(`usuarios/validacion/tutor?correo=${email}&idUnidad=${idUnit}`),
    verifyCoorEmail: (email) => axios.get(`usuarios/validacion/coordinador?correo=${email}`),
    getRoles: (email) => axios.get(`usuarios/roles?correo=${email}`),
    updateUser: (idUser,user) => axios.put(`usuarios/${idUser}`,user),
    authenticateUser: (payload) => axios.post('/usuarios/login', payload),
    authenticateGoogleUser: (payload) => axios.post('/usuarios/login/google', payload),
    registerUser: (payload) => axios.post('/usuarios/registro', payload),
    confirmUser: (payload) => axios.post('/usuarios/confirmar', payload),

    //STUDENTS
    getStudents: (search = '') => axios.get(`alumnos?search=${search}`),
    addStudent: (student) => axios.post('alumnos', student),
    bulkStudents: (payload) => axios.post('alumnos?bulk=true', payload),
    getStudent: (id_student) => axios.get(`alumnos/${id_student}`),
    getStudentsByUnit: (idUnit,value) => axios.get(`unidades/${idUnit}/alumnos?search=${value}`),

    // TUTORS
    getTutors: (idUnit, search) => axios.get(`tutores?idUnidad=${idUnit}&search=${search}`),
    getTutor: (idTutor) => axios.get(`tutores/${idTutor}`),
    addTutor: (data) => axios.post('tutores', data),
    getTutorUnits: (idTutor) => axios.get(`unidades?idTutor=${idTutor}`),

    // COORDINATOR
    getCoordinators: (search = '') => axios.get(`coordinadores?search=${search}`),
    getCoordinator: (idCoordinator) => axios.get(`coordinadores/${idCoordinator}`),
    addCoordinator: (coordinator) => axios.post('coordinadores', coordinator),

    // UNITS
    addUnitProgram: (program) => axios.post('unidades/programas', program),
    addUnitFaculty: (faculty) => axios.post('unidades/facultad', faculty),
    getUnits: () => axios.get('unidades'),
    getUnitsByUnitId: (idUnit) => axios.get(`unidades/${idUnit}`),
    getUnitsPrograms: (idUnit) => axios.get(`unidades/${idUnit}`),
    getFaculties: (search = '') => axios.get(`unidades/facultad?search=${search}`),
    updateUnit: (idUnit,unit) => axios.put(`unidades/${idUnit}`,unit), //datos generales & coordinador

    //TUTORSHIPS
    getTutorship: (idTutorship) => axios.get(`tutorias/${idTutorship}`),
    getTutorships: (idUnit, search = '') => axios.get(`tutorias/todos?idUnidad=${idUnit}&search=${search}`),
    getStudentTutorships: (idStudent, search = '') => axios.get(`tutorias?idAlumno=${idStudent}&search=${search}`),
    getSupportUnits: () => axios.get('unidades-apoyo'),
    // estos recursos pueden ser utilizados para una tutoria en general, no solo tutorias individuales
    getStudentsInTutorship: (idTutorship, searchQuery) => axios.get(`tutorias/individual/${idTutorship}/alumnos?search=${searchQuery}&which=all`),
    getStudentsToAddInTutorship: (idTutorship, searchQuery) => axios.get(`tutorias/individual/${idTutorship}/alumnos?search=${searchQuery}&which=others`),
    addStudentsInTutorship: (idTutorship, idStudents) => axios.post(`tutorias/individual/${idTutorship}/alumnos`, {idAlumnos: idStudents}),
    // estos recursos pueden ser utilizados para una tutoria en general, no solo tutorias individuales
    getTutorsInTutorship: (idTutorship, searchQuery) => axios.get(`tutorias/individual/${idTutorship}/tutores?search=${searchQuery}&which=all`),
    getTutorsToAddInTutorship: (idTutorship, searchQuery) => axios.get(`tutorias/individual/${idTutorship}/tutores?search=${searchQuery}&which=others`),
    addTutorsInTutorship: (idTutorship, idUnit, idTutors) => axios.post(`tutorias/individual/${idTutorship}/tutores`, {idUnidad: idUnit, idTutores: idTutors}),
    getTutorsForStudentInTutorship: (idTutorship, idStudent, esCoord=true) => axios.get(`tutorias/individual/${idTutorship}/alumnos/${idStudent}/tutores?esCoord=${esCoord}`),

    // TUTORSHIP - MOMENTS
    addMomentSession: (session) => axios.post('tutorias/momento/sesiones', session),
    getMomentSessionStudents: (idTutor, search = '') => axios.get(`tutorias/momento/tutores/${idTutor}/alumnos?search=${search}`),

    // TUTORSHIP - INDIVIDUALS
    addTutorship: (data) => axios.post('tutorias/individual', data),
    updateTutorship: (idTutorship, data) => axios.put(`tutorias/${idTutorship}`, data),
    getTutoshipSessions: (idTutoship) => axios.get(`tutorias/individual/${idTutoship}/sesiones`),
    getStudentTutorshipSession: (idTutorship,idStudent) => axios.get(`tutorias/individual/${idTutorship}/${idStudent}/sesiones`),
    updateTutorshipSession: (idTutoship, idAlumno, idSession, data) => axios.put(`tutorias/individual/${idTutoship}/${idAlumno}/sesiones/${idSession}`, data),
    deleteTutorshipTutor: (idTutorship, idTutor) => axios.delete(`tutorias/individual/${idTutorship}/tutores/${idTutor}`),
    deleteTutorshipStudent: (idTutorship, idStudent) => axios.delete(`tutorias/individual/${idTutorship}/alumnos/${idStudent}`),

    // SESSIONS
    getStatus: () => axios.get('sesiones/estados'),
    getSession: (idSession) => axios.get(`sesiones/${idSession}`),
    getHistorical: (idStudent) => axios.get(`sesiones?idAlumno=${idStudent}`),
    getTutorSessions: (idTutor, search = '') => axios.get(`citas/historial?idTutor=${idTutor}&search=${search}`),
    updateSession: (idSession, data) => axios.put(`sesiones/${idSession}`, data),
    getAppointments: (idStudent) => axios.get(`citas/${idStudent}`),
    registerAppointment: (data) => axios.post('sesiones', data),
    getSessionsIndividual:(idTutorship, idAlumno) => axios.get(`sesiones?idTutoria=${idTutorship}&idAlumno=${idAlumno}`),
    getSessionsMoment:(idTutor, idAlumno) => axios.get(`sesiones?idTutor=${idTutor}&idAlumno=${idAlumno}`),
    calificateSession:(calification) => axios.post('/sesiones/indicadores', calification),

    // TUTORSHIP TYPES (UNUSED?)
    addTutorshipType: (tutorshipType) => axios.post('tipos-tutoria', tutorshipType),
    getTutorshipTypes: (search = '', idUnit) => axios.get(`tipos-tutoria?search=${search}&id_unit=${idUnit}`),

    //DISPONIBILIDAD
    addTutorAvailability: (idTutor, data) => axios.put(`tutores/${idTutor}/disponibilidad`, data),
    getTutorAvailability: (idTutor, noDisp, ini, fin) => axios.get(`tutores/${idTutor}/disponibilidad?listarNoAprobados=${noDisp}&fechaInicio=${ini}&fechaFinal=${fin}`),
    replicateTutorAvailability: (idTutor, data) => axios.put(`tutores/${idTutor}/disponibilidad/replicar`, data),

    //FILES
    addFileStudent: (idStudent, data) => axios.put(`alumnos/${idStudent}/info`,data),
    getFileStudent: (idStudent) => axios.get(`alumnos/${idStudent}/info`),
    getTutorshipReport: (idUnit, fechaIni, fechaFin) => axios.get(`reportes/${idUnit}/tutorias?fechaIni=${fechaIni}&fechaFin=${fechaFin}`),
    getMometTutorshipReport: (idUnit, fechaIni, fechaFin) => axios.get(`reportes/${idUnit}/tutoriasalmomento?fechaIni=${fechaIni}&fechaFin=${fechaFin}`),
    getTutorsReport: (idUnit, fechaIni, fechaFin) => axios.get(`reportes/${idUnit}/tutores?fechaIni=${fechaIni}&fechaFin=${fechaFin}`),
    deleteFileStudent: (idStudent, idInfo) => axios.delete(`alumnos/${idStudent}/info/${idInfo}`),

    //UNIVERSIDAD
    updateUniversity: (data) => axios.put('universidad', data),
    getUniversity:() => axios.get('universidad')
  });
}
